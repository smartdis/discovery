#!/usr/bin/perl

# Copyright 2013 Tormod Volden
#
# A disassembler tool for ARM Cortex which follows the code execution
# paths and identifies data locations
# Also generates a dot (graphviz) call graph file.
#
# Usage: $0 filename-base offset max-address
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

$debug = 1;
$objdump = 'arm-none-eabi-objdump -M force-thumb';

# only inspect every 2^$ALIGN byte location
# use 1 for Thumb code (or 2 for ARM code)
$ALIGN = 1;

$CODE = 1;		# disassembled code
$DATA = 2;		# inlined data
$VECTOR = 4;		# pointer to code
$CONTINUED = 8;		# part of larger instruction (i.e. 4 bytes on Thumb)
$END = 16;		# known end of code
$BRANCH = 32;		# branch target
$END_POINT = 64;	# execution flow leaves here
$ENTRY_POINT = 128;	# execution starts here

$filename_base = $ARGV[0];
$offset = hex($ARGV[1]);
$max_address = hex($ARGV[2]);

sub data_at;
sub disabit;
sub load_labels;
sub in_range;

# Slurp in the binary file
{
	open (BIN, '<', "$filename_base.bin") or
				die "can not open $filename_base.bin: $!";
	binmode BIN;
	local $/;
	$bin = <BIN>;
	close (BIN);
}

$bin_len = length($bin);
printf "size of binary = %i\n", $bin_len;
@words = unpack("I*", $bin);

$max_address = $offset + $bin_len unless $max_address;

# ARM vector table, on Cortex starts with stack pointer and reset vector
$stack_pointer = $words[0];
$reset_vector = $words[1];
$loc[$max_address - $offset >> $ALIGN] = $END;
$loc[$reset_vector - $offset >> $ALIGN] = $ENTRY_POINT;
printf "stack at %08x, reset at %08x\n", $stack_pointer, $reset_vector;

load_labels($filename_base, $offset);

# call graph
open (DOT, '>', "$filename_base.dot") or
				die "can not create $filename_base.dot: $!";
print DOT "digraph $filename_base {\n";

# add entry points from vector table

foreach my $vect_index (2 .. $#words) {
	$address = $words[$vect_index];
	next if not $address;
	printf "vector $vect_index contains %08x\n", $address if $debug;
	if ($address > $offset && $address < $offset + $bin_len) {
		$loc[$address - $offset >> $ALIGN] = $ENTRY_POINT;
		$loc[$vect_index * 4 >> $ALIGN] = $VECTOR;
	} else {
		# end of vector table?
		print "vector table seems to contain $vect_index entries\n";
		last;
	}
}

# disassemble all known entry points
$new_branches = 0;
print "First pass: Use known execution code entries\n";
foreach my $idx (0 .. $#loc) {

	if ($loc[$idx] == $ENTRY_POINT) {
		my $address = $offset + ($idx << $ALIGN);
		printf "disassemble entry point %08x\n", $address if $debug;
		disabit("$filename_base" . '.elf', $address, $max_address);
	}
}

$pass = 1;
while ($new_branches) {
	$pass += 1;
	print "Pass $pass: Check discovered branch targets\n";
	$new_branches = 0;
	foreach my $idx (0 .. $#loc) {
		if (($loc[$idx] & $BRANCH) && !($loc[$idx] & $CODE)) {
			my $address = $offset + ($idx << $ALIGN);
			printf "disassemble branch target %08x\n", $address if $debug;
			disabit("$filename_base" . '.elf', $address, $max_address);
		}
	}
}

print DOT "}\n";
close(DOT);

print "Result of code discovery\n";
foreach my $idx (0 .. $#loc) {
	next if ($loc[$idx] & $CONTINUED );
	my $address = $offset + ($idx << $ALIGN);
	my $label;
	$label = "b_" if $loc[$idx] & $BRANCH;
	$label = "proc_" if $loc[$idx] & $ENTRY_POINT;
	$label .= $labels{$address};
	if ($loc[$idx] & $CODE) {
		printf "x %08x\t$label\t%s\n", $address, $disass[$idx];
		next;
	} elsif ($idx & 1 && $ALIGN == 1) {
		# non-code must be word-aligned
		next;
	}
	if ($loc[$idx] & $DATA) {
		printf "d %08x\t%08x\n", $address, data_at($address);
	} elsif ($loc[$idx] & $VECTOR) {
		printf "v %08x\t%08x\n", $address, data_at($address);
	} else {
		printf "  %08x\t%08x\t", $address, data_at($address);
		# unknown content, print ascii dump as well
		for my $i (0..3) {
			my $c = substr($bin, $address + $i - $offset, 1);
			if ($c !~ /[[:graph:] ]/ ) {
				$c = '.';
			}
			print $c;
		}
		printf "\n";
	}
}


# Small convenience function
sub data_at {
	my $address = $_[0];
	return $words[$address - $offset >> 2];
}

# Tags a code location as being a branch target
# Arguments: branch-target-address
sub add_branch {
	my $address = $_[0];
	printf "add branch address %08x\n", $address if $debug;
	$loc[$address - $offset >> $ALIGN] |= $BRANCH;
	$new_branches = 1;
	push @branches_from_here, $address;
}

# Tags a code location as being a branch-and-link target (subroutine).
# Also produces call graph data.
# Arguments: caller-start-address branch-target-address
sub add_func {
	my $caller = $_[0];
	my $address = $_[1];
	printf "add proc address %08x\n", $address if $debug;
	$loc[$address - $offset >> $ALIGN] |= $ENTRY_POINT | $BRANCH;
	$new_branches = 1;
	printf "graph call from %08x -> %08x\n", $caller, $address if $debug;
	$call_label = $labels{$caller};
	$call_label = sprintf "f%08x", $caller unless $call_label;
	$addr_label = $labels{$address};
	$addr_label = sprintf "f%08x", $address unless $addr_label;
	printf DOT " $call_label -> $addr_label ;\n";
}

# Tags a location as storing data (non code)
# Arguments: address
sub add_data {
	my $address = $_[0];
	$loc[$address - $offset >> $ALIGN] |= $DATA;
	$loc[$address + 2 - $offset >> $ALIGN] |= $CONTINUED;
	printf "add data location %08x\n", $address if $debug;
}

# Tries to deduct the value of a register by walking the code backwards.
# Arguments: address register-name
sub deduct_reg_value {
	my $address = $_[0];
	my $reg = $_[1];
	undef my $value;
	WALKBACK: {		# double block to make "last" work with do-until
	    do {
		$address -= 1 << $ALIGN;
		#printf "looking for $reg value at %08x\n", $address if $debug;
		my $tag = $loc[($address - $offset) >> $ALIGN ];
		last if $tag & $END_POINT;
		last if not $tag & ($CODE | $CONTINUED);
		my $up = $disass[ ($address - $offset) >> $ALIGN ];
		# TODO: also look for decoded movw/movt ?
		if ($up =~ /^ldr\t$reg, \[pc, #-?\d+\]\t; \((\w+)/ ) {
			$data_address = hex($1);
			printf "found $reg read from %08x ", $data_address if $debug;
			$value = data_at($data_address);
			printf "with value %08x\n", $value if $debug;
		} elsif ($up =~ /\t$reg, / ) {
			# if register is changed in other ways
			printf "manipulated $reg at %08x, giving up\n", $address;
			last WALKBACK;
		}
	    } until defined $value;
	    return $value;
	}
}

# Disassembles a chunk of code, from a given start address, until it
# reaches an unconditional branch or return instruction,
# Arguments: elf-file start-address max-address
sub disabit {

	$elf_file = $_[0];
	$start = $_[1];

	if (defined $_[2]) {
		$max_address = $_[2];
	}

	@branches_from_here = ();

	open (DISASS, "$objdump -d --start-address $start \"$elf_file\" |")
		or die "can not open $elf_file: $!";

	LINE: while(<DISASS>) {
		my $leave;
		next if not /^ [[:xdigit:]]+:/;
		print $_ if $debug;
		($haddress, $opbytes, $code) = ( $_ =~ /^ (\w+):\t([ \w]+)\t(\w.*)/ );
		$address = hex($haddress);
		last if ($address >= $max_address);
		if ($loc[$address - $offset >> $ALIGN] & $DATA) {
			print "error: address $haddress was tagged as data!\n";
			last;
		}
		$loc[$address - $offset >> $ALIGN] |= $CODE;
		$opbytes =~ s/ //g;
		my $nbytes = length($opbytes) >> 1;
		if ($nbytes > 2) {
		    print "long instruction of length = $nbytes\n" if $debug;
		    for my $c_idx (1 .. ($nbytes>>1) - 1) {
			$loc[($address + $c_idx*2) - $offset >> $ALIGN] |= $CONTINUED;
			printf "marked continued code at %08x\n", $address + $c_idx*2 if $debug;
		    }
		}

		# ldr rX, =0xXXXXXXXX constructs
		if ( $code =~ /^ldr\t(r\d), \[pc, #-?\d+\]\t; \((\w+)/ ) {
			my $reg = $1;
			add_data( hex($2) );
			my $value = data_at(hex($2));
			$code = $& . ") $reg = " . (sprintf "%08x", $value);
			my $range = in_range( $value, $start );
			$code .= " $range" if $range;
		}
		# movw rX, #XXXX, movt rX #XXXX constructs
		if ( $code =~ /^movt\t(r\d), #\d+\t; 0x(\w+)/ ) {
			my $reg = $1;
			my $msb = $2;
			my $up = $disass[ ($address - 4 - $offset) >> $ALIGN ];
			if ( $up =~ /^movw\t$reg, #\d+\t; 0x(\w+)/ ) {
				my $value = hex($msb . $1);
				printf "equivalent to ldr $reg, =0x%08x\n", $value;
				$code .= sprintf " $reg = %08x", $value;
				my $range = in_range( $value, $start );
				$code .= " $range" if $range;
			}
		}

		$disass[ ($address - $offset) >> $ALIGN ] = $code;
		#print "\t\t\t\t$code\n" if $debug;

		# various conditional branches
		if ( $code =~ /^cbn?z\t\w+, (\w+) / ) {
			add_branch( hex($1) );
		}
		if ( $code =~ /^b[encmpvhlga]\w\.[nw]\t(\w+) / ) {
			add_branch( hex($1) );
		}

		# branch and link (returning)
		if ( $code =~ /^bl\t(\w+) / ) {
			add_func( $start, hex($1) & ~1 );
		}

		# try to interpret
		# ldr     r4, [pc, #8]  blx     r4
		if ( $code =~ /^blx\t(\w+)/ ) {
			printf "predict blx $1 at %08x\n", $address if $debug;
			my $value = deduct_reg_value($address, $1);
			if ($value) {
				add_func( $start, $value & ~1);
			} else {
				printf "failed to predict blx $1 at %08x\n", $address;
			}
		}
		# TODO: tbb     [pc, r1]

		# branches with no return
		if ( $code =~ /^b\.[nw]\t(\w+) / ) {
			add_branch( hex($1) );
			$leave = 1;
		}
		# try to interpret bx reg
		if ( $code =~ /^bx\t(r\d)/ ) {
			printf "predict bx $1 at %08x\n", $address if $debug;
			my $value = deduct_reg_value($address, $1);
			if ($value) {
				add_branch( $value );
			} else {
				printf "failed to predict bx $1 at %08x\n", $address;
			}
			$leave = 1;
		}
		$leave = 1 if $code =~ /^bx\tlr/;
		$leave = 1 if $code =~ /^pop\t.*pc/;
		$leave = 1 if $code =~ /^ldmia\.w\tsp.*pc/;
		# check if exection continues through branch
		my $next_address = $address + $nbytes;
		if ($leave) {
			printf "leave before %08x?\n", $next_address if $debug;
			foreach (@branches_from_here) {
				next LINE if ($_ == $next_address);
			}
			print "leave execution here\n" if $debug;
			last;
		}
	}
	$loc[($address - $offset) >> $ALIGN ] |= $END_POINT;
	close(DISASS);
}

# Load labels from file
# Arguments: filename_base offset
sub load_labels {
	my $label_file = $_[0] . '.labels';
	my $offset = $_[1];
	open (LABELS, '<', $label_file) or
				die "can not open $label_file: $!";
	while (<LABELS>) {
		next if /^#/;
		next if /^\s+$/;
		($plus, $haddress, $star, $label) = ( $_ =~ /(\+?)(\S+)\s+(\*?)(\S+)/ );
		# range of addresses
		if ( $haddress =~ /(\w+),(\+?)(\w+)/ ) {
			my $start_address = hex($1);
			my $end_address = hex($3);
			$end_address += $start_address if $2;
			printf "add range %s between %08x and %08x\n", $label, $start_address, $end_address if $debug;
			push @ranges, [ $start_address, $end_address, $label ];
			next;
		}
		$address = ($plus?$offset:0) + hex($haddress);
		printf "label $label at %08x%s\n", $address, ($star?" (vector)":"") if $debug;
		if ($star) {
			# all vectors must be inside binary
			if ($address >= $offset && $address < $offset + $bin_len) {
				$loc[$address - $offset >> $ALIGN] |= $VECTOR;
				$address = data_at($address);
			} else {
				printf "vector label $label outside binary at %08x\n", $address;
				next;
			}
		}
		$labels{$address & ~1} = $label;
	}
	close (LABELS);
}

sub in_range {
	my $address = $_[0];
	my $referer = $_[1];

	foreach $rangeref (@ranges) {
		@range = @$rangeref;
		#printf "check range %s between %08x and %08x\n", $range[2], $range[0], $range[1] if $debug;
		if ($address >= $range[0] && $address < $range[1]) {
			if ($referer) {
				$ref_label = $labels{$referer};
				$ref_label = sprintf "f%08x", $referer unless $ref_label;
				printf DOT " $ref_label -> %s\n", $ref_label . $range[2];
				printf DOT " %s [shape=box,label=\"%s\"]\n", $ref_label . $range[2], $range[2];
			}
			return $range[2];
		}
	}
	return;
}
