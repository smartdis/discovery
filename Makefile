
NAME = d
OFFSET = 0x08000000

ENTRY = 0x$(shell od -X $(NAME).bin | head -1 | cut -d' ' -f3)
STACK = 0x$(shell od -X $(NAME).bin | head -1 | cut -d' ' -f2)

all: $(NAME).png

$(NAME).elf: $(NAME).bin
	arm-none-eabi-objcopy --change-addresses=$(OFFSET) \
		--set-start $(ENTRY) \
		-I binary -O elf32-littlearm -B armv5 $< $@
	arm-none-eabi-objcopy --set-section-flags .data=code $@

$(NAME).dot: $(NAME).elf $(NAME).bin
	./traverse.pl $(NAME) $(OFFSET)

$(NAME).png: $(NAME).dot
	dot -Tpng -o $(NAME).png $(NAME).dot

